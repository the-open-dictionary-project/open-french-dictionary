package cymru.prv.dictionary.french

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.DictionaryList
import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import java.util.function.Function

class FrenchDictionary(list: DictionaryList = DictionaryList()): Dictionary(
    list,
    "fr",
    mapOf(
        WordType.noun to Function { Word(it, WordType.noun) },
        WordType.verb to Function { Word(it, WordType.verb) }
    )
) {

    @Override
    override fun getVersion(): String {
        return "1.2-alpha-1"
    }

}
