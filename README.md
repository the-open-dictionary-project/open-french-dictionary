# Open French Dictionary

[![pipeline status](https://gitlab.com/prvInSpace/open-french-dictionary/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/open-french-dictionary/-/commits/master)
[![coverage report](https://gitlab.com/prvInSpace/open-french-dictionary/badges/master/coverage.svg)](https://gitlab.com/prvInSpace/open-french-dictionary/-/commits/master) 
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Open Dictionary: v1.2](https://img.shields.io/static/v1?label=Open-Dictionary&message=v1.2&color=informational&link=https://gitlab.com/prvInSpace/open-dictionary-library)](https://gitlab.com/prvInSpace/open-dictionary-library)
[![Discord](https://img.shields.io/discord/718008955040956456.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/UzaFmfV)

The French Language-pack is an extension that adds some French translations to the [Open Source Dictionary project](https://opensourcedict.org).
It is based on the [Open Dictionary](https://gitlab.com/prvInSpace/open-dictionary-library) library.
This means that at the moment it is not a full dictionary in its own right, though this might change in the future.

## Community

Due to the history of the project, most of the developer community is located in the projects [Discord server](https://discord.gg/UzaFmfV). If you got any questions, suggestions, you need any help, or if you just want to pop by to have a cup of coffee or tea, then feel free to join! You are more than welcome to pop by!

The main website for the project is [opensourcedict.org](https://opensourcedict.org).
Here you'll find an interface that allows you to access the different dictionaries, download them as JSON files, and learn more about the project.

## Importing the Library using Gradle

The latest version of the library is hosted through the package repository here on Gitlab in due time.

## Maintainers

* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;

